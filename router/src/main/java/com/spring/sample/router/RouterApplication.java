package com.spring.sample.router;

import com.spring.sample.router.filter.UserTestFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RouterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RouterApplication.class, args);
    }


    @Bean
    public GlobalFilter customFilter() {
        return new UserTestFilter();
    }

    //代码可以是配置路由
    /*@Bean
    public RouteLocator routeLocator (RouteLocatorBuilder locatorBuilder) {

        return locatorBuilder.routes()
                .route(p ->
                        p.path("/xxoo")
                                .filters(f -> f.stripPrefix(1))
                                .uri("http://mashibing.com")
                )

                .route(p ->

                        p.path("/go")
                                .filters(f -> f.stripPrefix(1))
                                .uri("lb://MDB")
                )

                .build();
    }*/

}
