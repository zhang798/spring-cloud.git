package com.spring.sample.router.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.List;

/**
 * 拦截器定义
 *
 *  拦截器生效还需要添加到GlobalFilter bean容器
 *      @Bean
 *     public GlobalFilter customFilter() {
 *         return new UserTestFilter();
 *     }
 *
 *  拦截器可以鉴权
 *  日志
 *  链路追踪
 *
 *
 */
@Component
public class UserTestFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        MultiValueMap<String, String> queryParams = serverHttpRequest.getQueryParams();
        List<String> list = queryParams.get("testName");
        if (null == list || list.size() ==0) {

            // 非法请求
            System.out.println("拦截");
//			exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
//			return exchange.getResponse().setComplete();
            DataBuffer dataBuffer = exchange.getResponse().bufferFactory().wrap("没有testName".getBytes());
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);

            return exchange.getResponse().writeWith(Mono.just(dataBuffer));

        }

        return chain.filter(exchange);
    }

    //优选级 越大越后执行
    @Override
    public int getOrder() {
        return 0;
    }
}
