package com.spring.sample.router.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class RateLimitConfig {


    /**
     * 限流参数条件
     * @return
     */
    @Bean
    KeyResolver userKeyResolver() {
        //参数中必须有testName
        return exchange -> Mono.just(exchange.getRequest().getQueryParams().getFirst("testName"));
    }
}
