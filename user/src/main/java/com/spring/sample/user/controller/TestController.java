package com.spring.sample.user.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @RequestMapping(path = "/test1" ,method = RequestMethod.GET)
    public String test(){

        return "testUser1" ;
    }

    @RequestMapping(path = "/test" ,method = RequestMethod.POST)
    public String test(@RequestParam String testName){

        return "testUser"+testName ;
    }

    @RequestMapping(path = "/v2/test" ,method = RequestMethod.POST)
    public String testV2(@RequestParam String testName){

        return "testUserV2"+testName ;
    }

}
